EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text GLabel 1900 4350 2    50   Input ~ 0
2.2
Text GLabel 1900 4550 2    50   Input ~ 0
2.6
Text GLabel 1900 4750 2    50   Input ~ 0
2.10
Text GLabel 1900 4850 2    50   Input ~ 0
2.12
Text GLabel 1900 4650 2    50   Input ~ 0
2.8
Text GLabel 1400 4750 0    50   Input ~ 0
2.9
Text GLabel 1400 4450 0    50   Input ~ 0
2.3
Text GLabel 1400 4550 0    50   Input ~ 0
2.5
Text GLabel 1400 4650 0    50   Input ~ 0
2.7
Text GLabel 1400 4850 0    50   Input ~ 0
2.11
Text GLabel 1900 4450 2    50   Input ~ 0
2.4
Text GLabel 1900 3800 2    50   Input ~ 0
1.52
Text GLabel 1900 3600 2    50   Input ~ 0
1.48
Text GLabel 1900 3700 2    50   Input ~ 0
1.50
Text GLabel 1400 4350 0    50   Input ~ 0
2.1
Text GLabel 1400 3800 0    50   Input ~ 0
1.51
Text GLabel 1400 3600 0    50   Input ~ 0
1.47
Text GLabel 1400 3700 0    50   Input ~ 0
1.49
Text GLabel 1900 3400 2    50   Input ~ 0
1.44
Text GLabel 1900 3300 2    50   Input ~ 0
1.42
Text GLabel 1900 3100 2    50   Input ~ 0
1.38
Text GLabel 1900 3200 2    50   Input ~ 0
1.40
Text GLabel 1900 3500 2    50   Input ~ 0
1.46
Text GLabel 2800 3800 0    50   Input ~ 0
1.51
Text GLabel 2800 4350 0    50   Input ~ 0
2.1
Text GLabel 2800 4450 0    50   Input ~ 0
2.3
Text GLabel 2800 4550 0    50   Input ~ 0
2.5
Text GLabel 3300 4450 2    50   Input ~ 0
2.4
Text GLabel 2800 3500 0    50   Input ~ 0
1.45
Text GLabel 2800 3700 0    50   Input ~ 0
1.49
Text GLabel 2800 3600 0    50   Input ~ 0
1.47
Text GLabel 2800 3400 0    50   Input ~ 0
1.43
Text GLabel 2800 3300 0    50   Input ~ 0
1.41
Text GLabel 3300 3300 2    50   Input ~ 0
1.42
Text GLabel 3300 3200 2    50   Input ~ 0
1.40
Text GLabel 2800 3200 0    50   Input ~ 0
1.39
Text GLabel 3300 3100 2    50   Input ~ 0
1.38
Text GLabel 3300 3500 2    50   Input ~ 0
1.46
Text GLabel 3300 3800 2    50   Input ~ 0
1.52
Text GLabel 3300 3700 2    50   Input ~ 0
1.50
Text GLabel 3300 3600 2    50   Input ~ 0
1.48
Text GLabel 3300 3400 2    50   Input ~ 0
1.44
Text GLabel 1400 5350 0    50   Input ~ 0
2.21
Text GLabel 1400 5250 0    50   Input ~ 0
2.19
Text GLabel 1400 5150 0    50   Input ~ 0
2.17
Text GLabel 1400 5050 0    50   Input ~ 0
2.15
Text GLabel 1400 4950 0    50   Input ~ 0
2.13
Text GLabel 1400 6850 0    50   Input ~ 0
2.51
Text GLabel 1400 6750 0    50   Input ~ 0
2.49
Text GLabel 1400 6650 0    50   Input ~ 0
2.47
Text GLabel 1400 6550 0    50   Input ~ 0
2.45
Text GLabel 1400 6450 0    50   Input ~ 0
2.43
Text GLabel 1400 6350 0    50   Input ~ 0
2.41
Text GLabel 1400 6250 0    50   Input ~ 0
2.39
Text GLabel 1400 6150 0    50   Input ~ 0
2.37
Text GLabel 1400 5650 0    50   Input ~ 0
2.27
Text GLabel 1400 5450 0    50   Input ~ 0
2.23
Text GLabel 1400 5550 0    50   Input ~ 0
2.25
Text GLabel 1400 5850 0    50   Input ~ 0
2.31
Text GLabel 1400 5750 0    50   Input ~ 0
2.29
Text GLabel 1400 5950 0    50   Input ~ 0
2.33
Text GLabel 1400 6050 0    50   Input ~ 0
2.35
Text GLabel 1900 6150 2    50   Input ~ 0
2.38
Text GLabel 1900 6250 2    50   Input ~ 0
2.40
Text GLabel 1900 5850 2    50   Input ~ 0
2.32
Text GLabel 1900 5750 2    50   Input ~ 0
2.30
Text GLabel 1900 5250 2    50   Input ~ 0
2.20
Text GLabel 1900 5350 2    50   Input ~ 0
2.22
Text GLabel 1900 5150 2    50   Input ~ 0
2.18
Text GLabel 1900 5050 2    50   Input ~ 0
2.16
Text GLabel 1900 4950 2    50   Input ~ 0
2.14
Text GLabel 1900 5450 2    50   Input ~ 0
2.24
Text GLabel 1900 5550 2    50   Input ~ 0
2.26
Text GLabel 1900 5650 2    50   Input ~ 0
2.28
Text GLabel 2800 6150 0    50   Input ~ 0
2.37
Text GLabel 2800 6250 0    50   Input ~ 0
2.39
Text GLabel 2800 6350 0    50   Input ~ 0
2.41
Text GLabel 2800 6450 0    50   Input ~ 0
2.43
Text GLabel 2800 6550 0    50   Input ~ 0
2.45
Text GLabel 2800 5850 0    50   Input ~ 0
2.31
Text GLabel 2800 6650 0    50   Input ~ 0
2.47
Text GLabel 2800 6750 0    50   Input ~ 0
2.49
Text GLabel 2800 6850 0    50   Input ~ 0
2.51
Text GLabel 3300 6850 2    50   Input ~ 0
2.52
Text GLabel 3300 6750 2    50   Input ~ 0
2.50
Text GLabel 3300 6650 2    50   Input ~ 0
2.48
Text GLabel 3300 6550 2    50   Input ~ 0
2.46
Text GLabel 3300 6450 2    50   Input ~ 0
2.44
Text GLabel 3300 6350 2    50   Input ~ 0
2.42
Text GLabel 2800 5950 0    50   Input ~ 0
2.33
Text GLabel 2800 6050 0    50   Input ~ 0
2.35
Text GLabel 1900 6350 2    50   Input ~ 0
2.42
Text GLabel 1900 5950 2    50   Input ~ 0
2.34
Text GLabel 1900 6050 2    50   Input ~ 0
2.36
Text GLabel 1900 6450 2    50   Input ~ 0
2.44
Text GLabel 1900 6550 2    50   Input ~ 0
2.46
Text GLabel 1900 6650 2    50   Input ~ 0
2.48
Text GLabel 1900 6750 2    50   Input ~ 0
2.50
Text GLabel 1900 6850 2    50   Input ~ 0
2.52
Text GLabel 3300 6250 2    50   Input ~ 0
2.40
Text GLabel 3300 6150 2    50   Input ~ 0
2.38
Text GLabel 3300 6050 2    50   Input ~ 0
2.36
Text GLabel 3300 5950 2    50   Input ~ 0
2.34
Text GLabel 3300 5850 2    50   Input ~ 0
2.32
Text GLabel 2800 5750 0    50   Input ~ 0
2.29
Text GLabel 2800 5650 0    50   Input ~ 0
2.27
Text GLabel 2800 5450 0    50   Input ~ 0
2.23
Text GLabel 2800 5550 0    50   Input ~ 0
2.25
Text GLabel 3300 5750 2    50   Input ~ 0
2.30
Text GLabel 2800 5250 0    50   Input ~ 0
2.19
Text GLabel 2800 5350 0    50   Input ~ 0
2.21
Text GLabel 2800 5150 0    50   Input ~ 0
2.17
Text GLabel 2800 5050 0    50   Input ~ 0
2.15
Text GLabel 2800 4950 0    50   Input ~ 0
2.13
Text GLabel 2800 4750 0    50   Input ~ 0
2.9
Text GLabel 2800 4850 0    50   Input ~ 0
2.11
Text GLabel 2800 4650 0    50   Input ~ 0
2.7
Text GLabel 3300 5350 2    50   Input ~ 0
2.22
Text GLabel 3300 5450 2    50   Input ~ 0
2.24
Text GLabel 3300 5250 2    50   Input ~ 0
2.20
Text GLabel 3300 5550 2    50   Input ~ 0
2.26
Text GLabel 3300 5150 2    50   Input ~ 0
2.18
Text GLabel 3300 5650 2    50   Input ~ 0
2.28
Text GLabel 3300 4950 2    50   Input ~ 0
2.14
Text GLabel 3300 5050 2    50   Input ~ 0
2.16
Text GLabel 3300 4350 2    50   Input ~ 0
2.2
Text GLabel 3300 4550 2    50   Input ~ 0
2.6
Text GLabel 3300 4650 2    50   Input ~ 0
2.8
Text GLabel 3300 4750 2    50   Input ~ 0
2.10
Text GLabel 3300 4850 2    50   Input ~ 0
2.12
Text GLabel 1400 3500 0    50   Input ~ 0
1.45
Text GLabel 1400 3400 0    50   Input ~ 0
1.43
Text GLabel 1400 3300 0    50   Input ~ 0
1.41
Text GLabel 1400 3100 0    50   Input ~ 0
1.37
Text GLabel 1400 3200 0    50   Input ~ 0
1.39
Text GLabel 1400 2900 0    50   Input ~ 0
1.33
Text GLabel 1400 3000 0    50   Input ~ 0
1.35
Text GLabel 1900 3000 2    50   Input ~ 0
1.36
Text GLabel 1900 2900 2    50   Input ~ 0
1.34
Text GLabel 1400 2800 0    50   Input ~ 0
1.31
Text GLabel 1900 2800 2    50   Input ~ 0
1.32
Text GLabel 1900 2700 2    50   Input ~ 0
1.30
Text GLabel 1900 2600 2    50   Input ~ 0
1.28
Text GLabel 1900 2400 2    50   Input ~ 0
1.24
Text GLabel 1900 2500 2    50   Input ~ 0
1.26
Text GLabel 1900 2300 2    50   Input ~ 0
1.22
Text GLabel 1400 2700 0    50   Input ~ 0
1.29
Text GLabel 1400 2600 0    50   Input ~ 0
1.27
Text GLabel 1400 2500 0    50   Input ~ 0
1.25
Text GLabel 1400 2400 0    50   Input ~ 0
1.23
Text GLabel 1400 2300 0    50   Input ~ 0
1.21
Text GLabel 1400 2200 0    50   Input ~ 0
1.19
Text GLabel 1400 1900 0    50   Input ~ 0
1.13
Text GLabel 1400 2000 0    50   Input ~ 0
1.15
Text GLabel 1400 2100 0    50   Input ~ 0
1.17
Text GLabel 1400 1800 0    50   Input ~ 0
1.11
Text GLabel 1900 2200 2    50   Input ~ 0
1.20
Text GLabel 1900 2100 2    50   Input ~ 0
1.18
Text GLabel 1900 1800 2    50   Input ~ 0
1.12
Text GLabel 1900 1900 2    50   Input ~ 0
1.14
Text GLabel 1900 2000 2    50   Input ~ 0
1.16
Text GLabel 1400 1500 0    50   Input ~ 0
1.5
Text GLabel 1900 1400 2    50   Input ~ 0
1.4
Text GLabel 1900 1300 2    50   Input ~ 0
1.2
Text GLabel 1400 1300 0    50   Input ~ 0
1.1
Text GLabel 1400 1700 0    50   Input ~ 0
1.9
Text GLabel 1400 1400 0    50   Input ~ 0
1.3
Text GLabel 1400 1600 0    50   Input ~ 0
1.7
Text GLabel 1900 1500 2    50   Input ~ 0
1.6
Text GLabel 1900 1600 2    50   Input ~ 0
1.8
Text GLabel 1900 1700 2    50   Input ~ 0
1.10
Text GLabel 2800 1800 0    50   Input ~ 0
1.11
Text GLabel 2800 1900 0    50   Input ~ 0
1.13
Text GLabel 2800 2600 0    50   Input ~ 0
1.27
Text GLabel 2800 2500 0    50   Input ~ 0
1.25
Text GLabel 2800 2400 0    50   Input ~ 0
1.23
Text GLabel 2800 2300 0    50   Input ~ 0
1.21
Text GLabel 2800 2200 0    50   Input ~ 0
1.19
Text GLabel 2800 2100 0    50   Input ~ 0
1.17
Text GLabel 2800 2000 0    50   Input ~ 0
1.15
Text GLabel 2800 1400 0    50   Input ~ 0
1.3
Text GLabel 2800 1500 0    50   Input ~ 0
1.5
Text GLabel 2800 1600 0    50   Input ~ 0
1.7
Text GLabel 2800 1700 0    50   Input ~ 0
1.9
Text GLabel 2800 1300 0    50   Input ~ 0
1.1
Text GLabel 2800 3000 0    50   Input ~ 0
1.35
Text GLabel 2800 3100 0    50   Input ~ 0
1.37
Text GLabel 2800 2900 0    50   Input ~ 0
1.33
Text GLabel 2800 2800 0    50   Input ~ 0
1.31
Text GLabel 2800 2700 0    50   Input ~ 0
1.29
Text GLabel 3300 2900 2    50   Input ~ 0
1.34
Text GLabel 3300 3000 2    50   Input ~ 0
1.36
Text GLabel 3300 2800 2    50   Input ~ 0
1.32
Text GLabel 3300 2700 2    50   Input ~ 0
1.30
Text GLabel 3300 2200 2    50   Input ~ 0
1.20
Text GLabel 3300 2000 2    50   Input ~ 0
1.16
Text GLabel 3300 2100 2    50   Input ~ 0
1.18
Text GLabel 3300 1900 2    50   Input ~ 0
1.14
Text GLabel 3300 1800 2    50   Input ~ 0
1.12
Text GLabel 4050 1525 0    50   Input ~ 0
2.45
Text GLabel 4050 1625 0    50   Input ~ 0
2.46
Text GLabel 3300 1700 2    50   Input ~ 0
1.10
Text GLabel 3300 1600 2    50   Input ~ 0
1.8
Text GLabel 3300 1300 2    50   Input ~ 0
1.2
Text GLabel 3300 1400 2    50   Input ~ 0
1.4
Text GLabel 3300 1500 2    50   Input ~ 0
1.6
Text GLabel 3300 2300 2    50   Input ~ 0
1.22
Text GLabel 3300 2400 2    50   Input ~ 0
1.24
Text GLabel 3300 2600 2    50   Input ~ 0
1.28
Text GLabel 3300 2500 2    50   Input ~ 0
1.26
Text GLabel 4050 1950 0    50   Input ~ 0
2.30
Text GLabel 4050 1850 0    50   Input ~ 0
2.29
Text GLabel 4050 2050 0    50   Input ~ 0
2.32
Wire Wire Line
	4050 1850 4100 1850
Wire Wire Line
	4100 1850 4100 1950
Wire Wire Line
	4700 1525 4750 1525
Wire Wire Line
	4750 1625 4700 1625
Wire Wire Line
	4750 1525 4750 1625
Wire Wire Line
	4750 1425 4750 1525
Connection ~ 4750 1525
Wire Wire Line
	4100 2050 4100 2150
Wire Wire Line
	4100 1950 4100 2050
Wire Wire Line
	4050 2050 4100 2050
Connection ~ 4100 2050
Wire Wire Line
	4050 1950 4100 1950
Wire Wire Line
	4425 1850 4425 2150
Connection ~ 4100 1950
Text GLabel 4375 1850 0    50   Input ~ 0
2.31
Wire Wire Line
	4375 1850 4425 1850
Text GLabel 4700 1625 0    50   Input ~ 0
2.28
Text GLabel 4700 1525 0    50   Input ~ 0
2.27
$Comp
L power:+BATT #PWR0122
U 1 1 5E526D9E
P 4100 1225
F 0 "#PWR0122" H 4100 1075 50  0001 C CNN
F 1 "+BATT" V 4115 1353 50  0000 L CNN
F 2 "" H 4100 1225 50  0001 C CNN
F 3 "" H 4100 1225 50  0001 C CNN
	1    4100 1225
	1    0    0    -1  
$EndComp
$Comp
L power:GNDA #PWR0124
U 1 1 5E6AE908
P 4425 2150
F 0 "#PWR0124" H 4425 1900 50  0001 C CNN
F 1 "GNDA" V 4425 1925 50  0000 C CNN
F 2 "" H 4425 2150 50  0001 C CNN
F 3 "" H 4425 2150 50  0001 C CNN
	1    4425 2150
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR018
U 1 1 5E5520D0
P 4750 1225
F 0 "#PWR018" H 4750 1150 50  0001 C CNN
F 1 "+3.3V" V 4736 1353 50  0000 L CNN
F 2 "" H 4750 1225 50  0001 C CNN
F 3 "" H 4750 1225 50  0001 C CNN
	1    4750 1225
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR017
U 1 1 5E53E935
P 4425 1225
F 0 "#PWR017" H 4425 1150 50  0001 C CNN
F 1 "+5V" V 4411 1353 50  0000 L CNN
F 2 "" H 4425 1225 50  0001 C CNN
F 3 "" H 4425 1225 50  0001 C CNN
	1    4425 1225
	1    0    0    -1  
$EndComp
Wire Wire Line
	4100 1625 4050 1625
Wire Wire Line
	4050 1525 4100 1525
Wire Wire Line
	4100 1525 4100 1625
Text GLabel 4375 1525 0    50   Input ~ 0
2.25
Wire Wire Line
	4425 1525 4425 1625
Wire Wire Line
	4375 1525 4425 1525
Wire Wire Line
	4100 1425 4100 1525
Wire Wire Line
	4425 1425 4425 1525
Connection ~ 4425 1525
Connection ~ 4100 1525
Text GLabel 4375 1625 0    50   Input ~ 0
2.26
Wire Wire Line
	4425 1625 4375 1625
$Comp
L power:GND #PWR0123
U 1 1 5E6510B7
P 4100 2275
F 0 "#PWR0123" H 4100 2025 50  0001 C CNN
F 1 "GND" V 4105 2147 50  0000 R CNN
F 2 "" H 4100 2275 50  0001 C CNN
F 3 "" H 4100 2275 50  0001 C CNN
	1    4100 2275
	1    0    0    -1  
$EndComp
$Comp
L Device:Jumper_NO_Small JP1
U 1 1 5E668409
P 4100 1325
F 0 "JP1" V 4100 1525 50  0000 R CNN
F 1 "Jumper_NC_Small" H 4100 1445 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 4100 1325 50  0001 C CNN
F 3 "~" H 4100 1325 50  0001 C CNN
	1    4100 1325
	0    -1   -1   0   
$EndComp
$Comp
L Device:Jumper_NO_Small JP2
U 1 1 5E68D70D
P 4425 1325
F 0 "JP2" V 4425 1525 50  0000 R CNN
F 1 "Jumper_NC_Small" H 4425 1445 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 4425 1325 50  0001 C CNN
F 3 "~" H 4425 1325 50  0001 C CNN
	1    4425 1325
	0    -1   -1   0   
$EndComp
$Comp
L Device:Jumper_NO_Small JP3
U 1 1 5E6922C9
P 4750 1325
F 0 "JP3" V 4750 1525 50  0000 R CNN
F 1 "Jumper_NC_Small" H 4750 1445 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 4750 1325 50  0001 C CNN
F 3 "~" H 4750 1325 50  0001 C CNN
	1    4750 1325
	0    -1   -1   0   
$EndComp
$Comp
L Connector_Generic:Conn_01x19 J6
U 1 1 5FC77839
P 8500 1875
F 0 "J6" H 8418 2992 50  0000 C CNN
F 1 "ESP32_1_R" H 8325 2900 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x19_P2.54mm_Vertical" H 8500 1875 50  0001 C CNN
F 3 "~" H 8500 1875 50  0001 C CNN
	1    8500 1875
	-1   0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x19_Odd_Even J8
U 1 1 5FC7F707
P 9500 1875
F 0 "J8" H 9550 2992 50  0000 C CNN
F 1 "Conn_02x19_Odd_Even" H 9550 2901 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x19_P2.54mm_Vertical" H 9500 1875 50  0001 C CNN
F 3 "~" H 9500 1875 50  0001 C CNN
	1    9500 1875
	1    0    0    -1  
$EndComp
Text GLabel 8700 975  2    50   Input ~ 0
6.1
Text GLabel 8700 1075 2    50   Input ~ 0
6.2
Text GLabel 8700 1175 2    50   Input ~ 0
6.3
Text GLabel 8700 1275 2    50   Input ~ 0
6.4
Text GLabel 8700 1375 2    50   Input ~ 0
6.5
Text GLabel 8700 1475 2    50   Input ~ 0
6.6
Text GLabel 8700 1575 2    50   Input ~ 0
6.7
Text GLabel 8700 1675 2    50   Input ~ 0
6.8
Text GLabel 8700 1775 2    50   Input ~ 0
6.9
Text GLabel 8700 1875 2    50   Input ~ 0
6.10
Text GLabel 8700 1975 2    50   Input ~ 0
6.11
Text GLabel 8700 2075 2    50   Input ~ 0
6.12
Text GLabel 8700 2175 2    50   Input ~ 0
6.13
Text GLabel 8700 2275 2    50   Input ~ 0
6.14
Text GLabel 8700 2375 2    50   Input ~ 0
6.15
Text GLabel 8700 2475 2    50   Input ~ 0
6.16
Text GLabel 8700 2575 2    50   Input ~ 0
6.17
Text GLabel 8700 2675 2    50   Input ~ 0
6.18
Text GLabel 8700 2775 2    50   Input ~ 0
6.19
Text GLabel 9300 975  0    50   Input ~ 0
6.1
Text GLabel 9300 1075 0    50   Input ~ 0
6.2
Text GLabel 9300 1175 0    50   Input ~ 0
6.3
Text GLabel 9300 1275 0    50   Input ~ 0
6.4
Text GLabel 9300 1375 0    50   Input ~ 0
6.5
Text GLabel 9300 1475 0    50   Input ~ 0
6.6
Text GLabel 9300 1575 0    50   Input ~ 0
6.7
Text GLabel 9300 1675 0    50   Input ~ 0
6.8
Text GLabel 9300 1775 0    50   Input ~ 0
6.9
Text GLabel 9300 1875 0    50   Input ~ 0
6.10
Text GLabel 9300 1975 0    50   Input ~ 0
6.11
Text GLabel 9300 2075 0    50   Input ~ 0
6.12
Text GLabel 9300 2175 0    50   Input ~ 0
6.13
Text GLabel 9300 2275 0    50   Input ~ 0
6.14
Text GLabel 9300 2375 0    50   Input ~ 0
6.15
Text GLabel 9300 2475 0    50   Input ~ 0
6.16
Text GLabel 9300 2575 0    50   Input ~ 0
6.17
Text GLabel 9300 2675 0    50   Input ~ 0
6.18
Text GLabel 9300 2775 0    50   Input ~ 0
6.19
Text GLabel 9800 975  2    50   Input ~ 0
6.1
Text GLabel 9800 1075 2    50   Input ~ 0
6.2
Text GLabel 9800 1175 2    50   Input ~ 0
6.3
Text GLabel 9800 1275 2    50   Input ~ 0
6.4
Text GLabel 9800 1375 2    50   Input ~ 0
6.5
Text GLabel 9800 1475 2    50   Input ~ 0
6.6
Text GLabel 9800 1575 2    50   Input ~ 0
6.7
Text GLabel 9800 1675 2    50   Input ~ 0
6.8
Text GLabel 9800 1775 2    50   Input ~ 0
6.9
Text GLabel 9800 1875 2    50   Input ~ 0
6.10
Text GLabel 9800 1975 2    50   Input ~ 0
6.11
Text GLabel 9800 2075 2    50   Input ~ 0
6.12
Text GLabel 9800 2175 2    50   Input ~ 0
6.13
Text GLabel 9800 2275 2    50   Input ~ 0
6.14
Text GLabel 9800 2375 2    50   Input ~ 0
6.15
Text GLabel 9800 2475 2    50   Input ~ 0
6.16
Text GLabel 9800 2575 2    50   Input ~ 0
6.17
Text GLabel 9800 2675 2    50   Input ~ 0
6.18
Text GLabel 9800 2775 2    50   Input ~ 0
6.19
$Comp
L Connector_Generic:Conn_01x19 J7
U 1 1 5FC993E2
P 8200 1875
F 0 "J7" H 8150 2975 50  0000 L CNN
F 1 "ESP32_1_L" H 7825 2900 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x19_P2.54mm_Vertical" H 8200 1875 50  0001 C CNN
F 3 "~" H 8200 1875 50  0001 C CNN
	1    8200 1875
	1    0    0    -1  
$EndComp
Text GLabel 8000 975  0    50   Input ~ 0
7.1
Text GLabel 8000 1075 0    50   Input ~ 0
7.2
Text GLabel 8000 1175 0    50   Input ~ 0
7.3
Text GLabel 8000 1275 0    50   Input ~ 0
7.4
Text GLabel 8000 1375 0    50   Input ~ 0
7.5
Text GLabel 8000 1475 0    50   Input ~ 0
7.6
Text GLabel 8000 1575 0    50   Input ~ 0
7.7
Text GLabel 8000 1675 0    50   Input ~ 0
7.8
Text GLabel 8000 1775 0    50   Input ~ 0
7.9
Text GLabel 8000 1875 0    50   Input ~ 0
7.10
Text GLabel 8000 1975 0    50   Input ~ 0
7.11
Text GLabel 8000 2075 0    50   Input ~ 0
7.12
Text GLabel 8000 2175 0    50   Input ~ 0
7.13
Text GLabel 8000 2275 0    50   Input ~ 0
7.14
Text GLabel 8000 2375 0    50   Input ~ 0
7.15
Text GLabel 8000 2475 0    50   Input ~ 0
7.16
Text GLabel 8000 2575 0    50   Input ~ 0
7.17
Text GLabel 8000 2675 0    50   Input ~ 0
7.18
Text GLabel 8000 2775 0    50   Input ~ 0
7.19
$Comp
L Connector_Generic:Conn_02x19_Odd_Even J9
U 1 1 5FCAE4E0
P 7100 1875
F 0 "J9" H 7150 2992 50  0000 C CNN
F 1 "Conn_02x19_Odd_Even" H 7150 2901 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x19_P2.54mm_Vertical" H 7100 1875 50  0001 C CNN
F 3 "~" H 7100 1875 50  0001 C CNN
	1    7100 1875
	1    0    0    -1  
$EndComp
Text GLabel 6900 975  0    50   Input ~ 0
7.1
Text GLabel 6900 1075 0    50   Input ~ 0
7.2
Text GLabel 6900 1175 0    50   Input ~ 0
7.3
Text GLabel 6900 1275 0    50   Input ~ 0
7.4
Text GLabel 6900 1375 0    50   Input ~ 0
7.5
Text GLabel 6900 1475 0    50   Input ~ 0
7.6
Text GLabel 6900 1575 0    50   Input ~ 0
7.7
Text GLabel 6900 1675 0    50   Input ~ 0
7.8
Text GLabel 6900 1775 0    50   Input ~ 0
7.9
Text GLabel 6900 1875 0    50   Input ~ 0
7.10
Text GLabel 6900 1975 0    50   Input ~ 0
7.11
Text GLabel 6900 2075 0    50   Input ~ 0
7.12
Text GLabel 6900 2175 0    50   Input ~ 0
7.13
Text GLabel 6900 2275 0    50   Input ~ 0
7.14
Text GLabel 6900 2375 0    50   Input ~ 0
7.15
Text GLabel 6900 2475 0    50   Input ~ 0
7.16
Text GLabel 6900 2575 0    50   Input ~ 0
7.17
Text GLabel 6900 2675 0    50   Input ~ 0
7.18
Text GLabel 6900 2775 0    50   Input ~ 0
7.19
Text GLabel 7400 975  2    50   Input ~ 0
7.1
Text GLabel 7400 1075 2    50   Input ~ 0
7.2
Text GLabel 7400 1175 2    50   Input ~ 0
7.3
Text GLabel 7400 1275 2    50   Input ~ 0
7.4
Text GLabel 7400 1375 2    50   Input ~ 0
7.5
Text GLabel 7400 1475 2    50   Input ~ 0
7.6
Text GLabel 7400 1575 2    50   Input ~ 0
7.7
Text GLabel 7400 1675 2    50   Input ~ 0
7.8
Text GLabel 7400 1775 2    50   Input ~ 0
7.9
Text GLabel 7400 1875 2    50   Input ~ 0
7.10
Text GLabel 7400 1975 2    50   Input ~ 0
7.11
Text GLabel 7400 2075 2    50   Input ~ 0
7.12
Text GLabel 7400 2175 2    50   Input ~ 0
7.13
Text GLabel 7400 2275 2    50   Input ~ 0
7.14
Text GLabel 7400 2375 2    50   Input ~ 0
7.15
Text GLabel 7400 2475 2    50   Input ~ 0
7.16
Text GLabel 7400 2575 2    50   Input ~ 0
7.17
Text GLabel 7400 2675 2    50   Input ~ 0
7.18
Text GLabel 7400 2775 2    50   Input ~ 0
7.19
$Comp
L Interface_CAN_LIN:SN65HVD230 U1
U 1 1 5FBE32F3
P 6475 4025
F 0 "U1" H 6475 4506 50  0000 C CNN
F 1 "SN65HVD230" H 6475 4415 50  0000 C CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 6475 3525 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/sn65hvd230.pdf" H 6375 4425 50  0001 C CNN
	1    6475 4025
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C1
U 1 1 5FBE6E1D
P 6475 3400
F 0 "C1" V 6246 3400 50  0000 C CNN
F 1 "100nF-10uF" V 6337 3400 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 6475 3400 50  0001 C CNN
F 3 "~" H 6475 3400 50  0001 C CNN
	1    6475 3400
	0    1    1    0   
$EndComp
$Comp
L power:+3.3V #PWR0101
U 1 1 5FBEC418
P 6300 3400
F 0 "#PWR0101" H 6300 3325 50  0001 C CNN
F 1 "+3.3V" V 6286 3528 50  0000 L CNN
F 2 "" H 6300 3400 50  0001 C CNN
F 3 "" H 6300 3400 50  0001 C CNN
	1    6300 3400
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6300 3400 6375 3400
$Comp
L power:GND #PWR0102
U 1 1 5FBF00A2
P 6650 3400
F 0 "#PWR0102" H 6650 3150 50  0001 C CNN
F 1 "GND" V 6655 3272 50  0000 R CNN
F 2 "" H 6650 3400 50  0001 C CNN
F 3 "" H 6650 3400 50  0001 C CNN
	1    6650 3400
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6575 3400 6650 3400
$Comp
L power:GND #PWR0103
U 1 1 5FBF1281
P 6475 4550
F 0 "#PWR0103" H 6475 4300 50  0001 C CNN
F 1 "GND" V 6480 4422 50  0000 R CNN
F 2 "" H 6475 4550 50  0001 C CNN
F 3 "" H 6475 4550 50  0001 C CNN
	1    6475 4550
	1    0    0    -1  
$EndComp
Wire Wire Line
	6475 4425 6475 4550
$Comp
L Device:R_Small R1
U 1 1 5FBF2294
P 6025 4375
F 0 "R1" H 6084 4421 50  0000 L CNN
F 1 "10k" H 6084 4330 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 6025 4375 50  0001 C CNN
F 3 "~" H 6025 4375 50  0001 C CNN
	1    6025 4375
	1    0    0    -1  
$EndComp
Wire Wire Line
	6025 4225 6075 4225
$Comp
L power:GND #PWR0104
U 1 1 5FBF2F5C
P 6025 4550
F 0 "#PWR0104" H 6025 4300 50  0001 C CNN
F 1 "GND" V 6030 4422 50  0000 R CNN
F 2 "" H 6025 4550 50  0001 C CNN
F 3 "" H 6025 4550 50  0001 C CNN
	1    6025 4550
	1    0    0    -1  
$EndComp
Wire Wire Line
	6025 4275 6025 4225
Wire Wire Line
	6025 4475 6025 4550
$Comp
L Device:R_Small R2
U 1 1 5FBF494C
P 7025 4075
F 0 "R2" H 7084 4121 50  0000 L CNN
F 1 "120" H 7084 4030 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 7025 4075 50  0001 C CNN
F 3 "~" H 7025 4075 50  0001 C CNN
	1    7025 4075
	1    0    0    -1  
$EndComp
Wire Wire Line
	6875 4025 6925 4025
Wire Wire Line
	6925 4025 6925 3975
Wire Wire Line
	6925 3975 7025 3975
Wire Wire Line
	6925 4175 7025 4175
Wire Wire Line
	6875 4125 6925 4125
Wire Wire Line
	6925 4125 6925 4175
$Comp
L Connector_Generic:Conn_01x02 J10
U 1 1 5FBF90C5
P 7675 4025
F 0 "J10" H 7755 4017 50  0000 L CNN
F 1 "Conn_01x02" H 7755 3926 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 7675 4025 50  0001 C CNN
F 3 "~" H 7675 4025 50  0001 C CNN
	1    7675 4025
	1    0    0    -1  
$EndComp
Wire Wire Line
	7475 4125 7375 4125
Wire Wire Line
	7375 4125 7375 4175
Wire Wire Line
	7375 4175 7025 4175
Connection ~ 7025 4175
Wire Wire Line
	7025 3975 7375 3975
Wire Wire Line
	7375 3975 7375 4025
Wire Wire Line
	7375 4025 7475 4025
Connection ~ 7025 3975
$Comp
L Connector_Generic:Conn_01x02 J11
U 1 1 5FC05E70
P 5650 3925
F 0 "J11" H 5568 3600 50  0000 C CNN
F 1 "Conn_01x02" H 5568 3691 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 5650 3925 50  0001 C CNN
F 3 "~" H 5650 3925 50  0001 C CNN
	1    5650 3925
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5850 3925 6075 3925
Wire Wire Line
	6075 4025 5850 4025
$Comp
L power:+3.3V #PWR0105
U 1 1 5FC43242
P 6425 3700
F 0 "#PWR0105" H 6425 3625 50  0001 C CNN
F 1 "+3.3V" V 6411 3828 50  0000 L CNN
F 2 "" H 6425 3700 50  0001 C CNN
F 3 "" H 6425 3700 50  0001 C CNN
	1    6425 3700
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6425 3700 6475 3700
Wire Wire Line
	6475 3700 6475 3725
$Comp
L Connector_Generic:Conn_02x26_Odd_Even J4
U 1 1 5E34ACC1
P 3000 2500
F 0 "J4" H 3000 4103 60  0000 C CNN
F 1 "HEADER_2x26" H 3000 3997 60  0000 C CNN
F 2 "ESP32_PC104:Spacemanic_header.pretty_Header_2x26_2.54mm" H 3000 3891 60  0001 C CNN
F 3 "" H 3000 3750 60  0000 C CNN
	1    3000 2500
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x26_Odd_Even J2
U 1 1 5D197155
P 1600 2500
F 0 "J2" H 1600 4103 60  0000 C CNN
F 1 "HEADER_2x26" H 1600 3997 60  0000 C CNN
F 2 "ESP32_PC104:Spacemanic_header.pretty_Header_2x26_2.54mm" H 1600 3891 60  0001 C CNN
F 3 "" H 1600 3750 60  0000 C CNN
	1    1600 2500
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x26_Odd_Even J3
U 1 1 5E34AA3A
P 3000 5550
F 0 "J3" H 3000 7153 60  0000 C CNN
F 1 "HEADER_2x26" H 3000 7047 60  0000 C CNN
F 2 "ESP32_PC104:Spacemanic_header.pretty_Header_2x26_2.54mm" H 3000 6941 60  0001 C CNN
F 3 "" H 3000 6800 60  0000 C CNN
	1    3000 5550
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x26_Odd_Even J1
U 1 1 5D194D38
P 1600 5550
F 0 "J1" H 1600 7153 60  0000 C CNN
F 1 "HEADER_2x26" H 1600 7047 60  0000 C CNN
F 2 "ESP32_PC104:Spacemanic_header.pretty_Header_2x26_2.54mm" H 1600 6941 60  0001 C CNN
F 3 "" H 1600 6800 60  0000 C CNN
	1    1600 5550
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R4
U 1 1 6000CD8D
P 4675 2900
F 0 "R4" H 4616 2854 50  0000 R CNN
F 1 "4k7" H 4616 2945 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 4675 2900 50  0001 C CNN
F 3 "~" H 4675 2900 50  0001 C CNN
	1    4675 2900
	-1   0    0    1   
$EndComp
$Comp
L Device:R_Small R3
U 1 1 6000E4EB
P 4425 2900
F 0 "R3" H 4366 2854 50  0000 R CNN
F 1 "4k7" H 4366 2945 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 4425 2900 50  0001 C CNN
F 3 "~" H 4425 2900 50  0001 C CNN
	1    4425 2900
	-1   0    0    1   
$EndComp
Wire Wire Line
	4425 2800 4425 2750
Wire Wire Line
	4425 2750 4675 2750
Wire Wire Line
	4675 2750 4675 2800
Wire Wire Line
	4675 2750 4675 2625
Connection ~ 4675 2750
Wire Wire Line
	4425 3000 4425 3075
Wire Wire Line
	4425 3075 4200 3075
Text GLabel 4200 3075 0    50   BiDi ~ 0
1.41
Text GLabel 4200 3175 0    50   BiDi ~ 0
1.43
Wire Wire Line
	4200 3175 4675 3175
Wire Wire Line
	4675 3000 4675 3175
$Comp
L Device:R_Small R6
U 1 1 600203B7
P 4675 3875
F 0 "R6" H 4616 3829 50  0000 R CNN
F 1 "4k7" H 4616 3920 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 4675 3875 50  0001 C CNN
F 3 "~" H 4675 3875 50  0001 C CNN
	1    4675 3875
	-1   0    0    1   
$EndComp
$Comp
L Device:R_Small R5
U 1 1 600203BD
P 4425 3875
F 0 "R5" H 4366 3829 50  0000 R CNN
F 1 "4k7" H 4366 3920 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 4425 3875 50  0001 C CNN
F 3 "~" H 4425 3875 50  0001 C CNN
	1    4425 3875
	-1   0    0    1   
$EndComp
Wire Wire Line
	4425 3775 4425 3725
Wire Wire Line
	4425 3725 4675 3725
Wire Wire Line
	4675 3725 4675 3775
Wire Wire Line
	4675 3725 4675 3600
Connection ~ 4675 3725
Wire Wire Line
	4425 3975 4425 4050
Wire Wire Line
	4425 4050 4200 4050
Text GLabel 4200 4050 0    50   BiDi ~ 0
1.23
Text GLabel 4200 4150 0    50   BiDi ~ 0
1.21
Wire Wire Line
	4200 4150 4675 4150
Wire Wire Line
	4675 3975 4675 4150
Text GLabel 4675 2625 1    50   Input ~ 0
2.27
Text GLabel 4675 3600 1    50   Input ~ 0
2.27
Text GLabel 7375 3975 1    50   BiDi ~ 0
1.3
Text GLabel 7375 4175 3    50   BiDi ~ 0
1.1
$Comp
L Mechanical:MountingHole_Pad H4
U 1 1 600725A5
P 9300 4275
F 0 "H4" H 9400 4324 50  0000 L CNN
F 1 "MountingHole_Pad" H 9400 4233 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_DIN965_Pad" H 9300 4275 50  0001 C CNN
F 3 "~" H 9300 4275 50  0001 C CNN
	1    9300 4275
	1    0    0    -1  
$EndComp
NoConn ~ 9300 3725
NoConn ~ 9300 4050
$Comp
L Device:L_Small L1
U 1 1 60077BEF
P 9025 3375
F 0 "L1" H 9073 3421 50  0000 L CNN
F 1 "L_Small" H 9073 3330 50  0000 L CNN
F 2 "ESP32_PC104:1210_handSolder" H 9025 3375 50  0001 C CNN
F 3 "~" H 9025 3375 50  0001 C CNN
	1    9025 3375
	0    1    -1   0   
$EndComp
$Comp
L power:GND #PWR01
U 1 1 60079513
P 8850 3375
F 0 "#PWR01" H 8850 3125 50  0001 C CNN
F 1 "GND" V 8855 3247 50  0000 R CNN
F 2 "" H 8850 3375 50  0001 C CNN
F 3 "" H 8850 3375 50  0001 C CNN
	1    8850 3375
	0    1    -1   0   
$EndComp
Wire Wire Line
	8925 3375 8850 3375
$Comp
L Mechanical:MountingHole_Pad H3
U 1 1 6007EFB6
P 9300 3950
F 0 "H3" H 9400 3999 50  0000 L CNN
F 1 "MountingHole_Pad" H 9400 3908 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_DIN965_Pad" H 9300 3950 50  0001 C CNN
F 3 "~" H 9300 3950 50  0001 C CNN
	1    9300 3950
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H2
U 1 1 6007F33C
P 9300 3625
F 0 "H2" H 9400 3674 50  0000 L CNN
F 1 "MountingHole_Pad" H 9400 3583 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_DIN965_Pad" H 9300 3625 50  0001 C CNN
F 3 "~" H 9300 3625 50  0001 C CNN
	1    9300 3625
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H1
U 1 1 6007F6CC
P 9300 3275
F 0 "H1" H 9400 3324 50  0000 L CNN
F 1 "MountingHole_Pad" H 9400 3233 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_DIN965_Pad" H 9300 3275 50  0001 C CNN
F 3 "~" H 9300 3275 50  0001 C CNN
	1    9300 3275
	1    0    0    -1  
$EndComp
Wire Wire Line
	9125 3375 9300 3375
NoConn ~ 9300 4375
$Comp
L Connector_Generic:Conn_02x20_Odd_Even J5
U 1 1 6001D9A6
P 5700 1900
F 0 "J5" H 5750 3017 50  0000 C CNN
F 1 "Conn_02x20_Odd_Even" H 5750 2926 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x20_P2.54mm_Vertical" H 5700 1900 50  0001 C CNN
F 3 "~" H 5700 1900 50  0001 C CNN
	1    5700 1900
	1    0    0    -1  
$EndComp
Text GLabel 5500 1900 0    50   Input ~ 0
2.29
Text GLabel 5500 2000 0    50   Input ~ 0
2.29
Text GLabel 5500 2100 0    50   Input ~ 0
2.29
Text GLabel 5500 2200 0    50   Input ~ 0
2.29
Text GLabel 5500 2300 0    50   Input ~ 0
2.29
Text GLabel 5500 2400 0    50   Input ~ 0
2.29
Text GLabel 5500 2500 0    50   Input ~ 0
2.29
Text GLabel 5500 2600 0    50   Input ~ 0
2.29
Text GLabel 5500 2700 0    50   Input ~ 0
2.29
Text GLabel 5500 2800 0    50   Input ~ 0
2.29
Text GLabel 5500 2900 0    50   Input ~ 0
2.29
Wire Wire Line
	5500 1000 5450 1000
Wire Wire Line
	5450 1000 5450 1100
Wire Wire Line
	5450 1800 5500 1800
Connection ~ 5450 1700
Wire Wire Line
	5450 1700 5450 1800
Wire Wire Line
	5450 1700 5500 1700
Wire Wire Line
	5500 1600 5450 1600
Connection ~ 5450 1600
Wire Wire Line
	5450 1600 5450 1700
Wire Wire Line
	5450 1500 5500 1500
Connection ~ 5450 1500
Wire Wire Line
	5450 1500 5450 1600
Wire Wire Line
	5500 1400 5450 1400
Connection ~ 5450 1400
Wire Wire Line
	5450 1400 5450 1500
Wire Wire Line
	5500 1300 5450 1300
Connection ~ 5450 1300
Wire Wire Line
	5450 1300 5450 1400
Wire Wire Line
	5450 1300 5450 1200
Wire Wire Line
	5450 1200 5500 1200
Connection ~ 5450 1200
Connection ~ 5450 1100
Wire Wire Line
	5450 1100 5450 1200
Wire Wire Line
	6000 1000 6075 1000
Wire Wire Line
	6000 1800 6075 1800
Wire Wire Line
	6000 1700 6075 1700
Connection ~ 6075 1700
Wire Wire Line
	6075 1700 6075 1800
Wire Wire Line
	6000 1600 6075 1600
Connection ~ 6075 1600
Wire Wire Line
	6075 1600 6075 1700
Wire Wire Line
	6075 1500 6000 1500
Connection ~ 6075 1500
Wire Wire Line
	6075 1500 6075 1600
Wire Wire Line
	6075 1400 6000 1400
Connection ~ 6075 1400
Wire Wire Line
	6075 1400 6075 1500
Wire Wire Line
	6000 1300 6075 1300
Wire Wire Line
	6075 1000 6075 1100
Connection ~ 6075 1300
Wire Wire Line
	6075 1300 6075 1400
Wire Wire Line
	6075 1200 6000 1200
Connection ~ 6075 1200
Wire Wire Line
	6075 1200 6075 1300
Wire Wire Line
	6000 1100 6075 1100
Connection ~ 6075 1100
Wire Wire Line
	6075 1100 6075 1200
$Comp
L power:+5V #PWR0107
U 1 1 6006CBA5
P 5425 1000
F 0 "#PWR0107" H 5425 925 50  0001 C CNN
F 1 "+5V" V 5411 1128 50  0000 L CNN
F 2 "" H 5425 1000 50  0001 C CNN
F 3 "" H 5425 1000 50  0001 C CNN
	1    5425 1000
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5425 1000 5450 1000
Connection ~ 5450 1000
$Comp
L power:+3.3V #PWR0108
U 1 1 60072CA4
P 6100 1000
F 0 "#PWR0108" H 6100 925 50  0001 C CNN
F 1 "+3.3V" V 6086 1128 50  0000 L CNN
F 2 "" H 6100 1000 50  0001 C CNN
F 3 "" H 6100 1000 50  0001 C CNN
	1    6100 1000
	0    1    1    0   
$EndComp
Wire Wire Line
	6100 1000 6075 1000
Connection ~ 6075 1000
Wire Wire Line
	5450 1100 5500 1100
Text GLabel 4050 2150 0    50   Input ~ 0
6.1
Wire Wire Line
	4050 2150 4100 2150
Wire Wire Line
	4100 2275 4100 2250
Connection ~ 4100 2150
Text GLabel 4050 2250 0    50   Input ~ 0
7.14
Wire Wire Line
	4050 2250 4100 2250
Connection ~ 4100 2250
Wire Wire Line
	4100 2250 4100 2150
Text GLabel 4725 1950 0    50   Input ~ 0
7.1
$Comp
L power:+3.3V #PWR0109
U 1 1 600BBC05
P 4825 1950
F 0 "#PWR0109" H 4825 1875 50  0001 C CNN
F 1 "+3.3V" V 4811 2078 50  0000 L CNN
F 2 "" H 4825 1950 50  0001 C CNN
F 3 "" H 4825 1950 50  0001 C CNN
	1    4825 1950
	0    1    1    0   
$EndComp
Wire Wire Line
	4725 1950 4825 1950
Wire Wire Line
	6000 1900 6050 1900
Wire Wire Line
	6050 2900 6000 2900
Wire Wire Line
	6000 2800 6050 2800
Connection ~ 6050 2800
Wire Wire Line
	6050 2800 6050 2900
Wire Wire Line
	6000 2700 6050 2700
Connection ~ 6050 2700
Wire Wire Line
	6050 2700 6050 2800
Wire Wire Line
	6050 1900 6050 2000
Connection ~ 6050 2600
Wire Wire Line
	6050 2600 6050 2700
Wire Wire Line
	6050 2600 6000 2600
Wire Wire Line
	6000 2500 6050 2500
Connection ~ 6050 2500
Wire Wire Line
	6050 2500 6050 2600
Wire Wire Line
	6000 2400 6050 2400
Connection ~ 6050 2400
Wire Wire Line
	6050 2400 6050 2500
Wire Wire Line
	6000 2300 6050 2300
Connection ~ 6050 2300
Wire Wire Line
	6050 2300 6050 2400
Wire Wire Line
	6050 2200 6000 2200
Connection ~ 6050 2200
Wire Wire Line
	6050 2200 6050 2300
Wire Wire Line
	6000 2100 6050 2100
Connection ~ 6050 2100
Wire Wire Line
	6050 2100 6050 2200
Wire Wire Line
	6000 2000 6050 2000
Connection ~ 6050 2000
Wire Wire Line
	6050 2000 6050 2100
$Comp
L power:+BATT #PWR0110
U 1 1 6011F19A
P 6050 1900
F 0 "#PWR0110" H 6050 1750 50  0001 C CNN
F 1 "+BATT" V 6065 2028 50  0000 L CNN
F 2 "" H 6050 1900 50  0001 C CNN
F 3 "" H 6050 1900 50  0001 C CNN
	1    6050 1900
	0    1    -1   0   
$EndComp
Connection ~ 6050 1900
$Comp
L Interface_UART:MAX3485 U2
U 1 1 6001D1AF
P 5700 5850
F 0 "U2" H 5700 6531 50  0000 C CNN
F 1 "MAX3485" H 5700 6440 50  0000 C CNN
F 2 "Package_SO:SO-8_5.3x6.2mm_P1.27mm" H 5700 5150 50  0001 C CNN
F 3 "https://datasheets.maximintegrated.com/en/ds/MAX3483-MAX3491.pdf" H 5700 5900 50  0001 C CNN
	1    5700 5850
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C2
U 1 1 600C2217
P 5700 5050
F 0 "C2" V 5471 5050 50  0000 C CNN
F 1 "100nF-10uF" V 5562 5050 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 5700 5050 50  0001 C CNN
F 3 "~" H 5700 5050 50  0001 C CNN
	1    5700 5050
	0    1    1    0   
$EndComp
$Comp
L power:+3.3V #PWR0106
U 1 1 600C221D
P 5525 5050
F 0 "#PWR0106" H 5525 4975 50  0001 C CNN
F 1 "+3.3V" V 5511 5178 50  0000 L CNN
F 2 "" H 5525 5050 50  0001 C CNN
F 3 "" H 5525 5050 50  0001 C CNN
	1    5525 5050
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5525 5050 5600 5050
$Comp
L power:GND #PWR0111
U 1 1 600C2224
P 5875 5050
F 0 "#PWR0111" H 5875 4800 50  0001 C CNN
F 1 "GND" V 5880 4922 50  0000 R CNN
F 2 "" H 5875 5050 50  0001 C CNN
F 3 "" H 5875 5050 50  0001 C CNN
	1    5875 5050
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5800 5050 5875 5050
$Comp
L power:GND #PWR0112
U 1 1 600C7601
P 5700 6525
F 0 "#PWR0112" H 5700 6275 50  0001 C CNN
F 1 "GND" V 5705 6397 50  0000 R CNN
F 2 "" H 5700 6525 50  0001 C CNN
F 3 "" H 5700 6525 50  0001 C CNN
	1    5700 6525
	1    0    0    -1  
$EndComp
Wire Wire Line
	5700 6450 5700 6525
Wire Wire Line
	6100 5750 6325 5750
Wire Wire Line
	6100 6050 6325 6050
$Comp
L Device:R_Small R8
U 1 1 600D5DCC
P 6325 5900
F 0 "R8" H 6384 5946 50  0000 L CNN
F 1 "120R" H 6384 5855 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.98x0.95mm_HandSolder" H 6325 5900 50  0001 C CNN
F 3 "~" H 6325 5900 50  0001 C CNN
	1    6325 5900
	1    0    0    -1  
$EndComp
Wire Wire Line
	6325 5800 6325 5750
Connection ~ 6325 5750
Wire Wire Line
	6325 6000 6325 6050
Connection ~ 6325 6050
Wire Wire Line
	6325 6050 6675 6050
$Comp
L power:+3.3V #PWR0113
U 1 1 600DFA8F
P 5600 5325
F 0 "#PWR0113" H 5600 5250 50  0001 C CNN
F 1 "+3.3V" V 5586 5453 50  0000 L CNN
F 2 "" H 5600 5325 50  0001 C CNN
F 3 "" H 5600 5325 50  0001 C CNN
	1    5600 5325
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5600 5325 5700 5325
Wire Wire Line
	5700 5325 5700 5350
$Comp
L Device:R_Small R7
U 1 1 600E4D67
P 6325 5600
F 0 "R7" H 6267 5554 50  0000 R CNN
F 1 "620R" H 6267 5645 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.98x0.95mm_HandSolder" H 6325 5600 50  0001 C CNN
F 3 "~" H 6325 5600 50  0001 C CNN
	1    6325 5600
	1    0    0    1   
$EndComp
Wire Wire Line
	6325 5700 6325 5750
$Comp
L Device:R_Small R9
U 1 1 600EF4FF
P 6325 6200
F 0 "R9" H 6267 6154 50  0000 R CNN
F 1 "620R" H 6267 6245 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.98x0.95mm_HandSolder" H 6325 6200 50  0001 C CNN
F 3 "~" H 6325 6200 50  0001 C CNN
	1    6325 6200
	1    0    0    1   
$EndComp
Wire Wire Line
	6325 6050 6325 6100
Wire Wire Line
	6325 6300 6325 6350
$Comp
L power:GND #PWR0114
U 1 1 600FE947
P 6325 5425
F 0 "#PWR0114" H 6325 5175 50  0001 C CNN
F 1 "GND" V 6330 5297 50  0000 R CNN
F 2 "" H 6325 5425 50  0001 C CNN
F 3 "" H 6325 5425 50  0001 C CNN
	1    6325 5425
	-1   0    0    1   
$EndComp
$Comp
L power:+3.3V #PWR0115
U 1 1 600FEE5E
P 6325 6350
F 0 "#PWR0115" H 6325 6275 50  0001 C CNN
F 1 "+3.3V" V 6311 6478 50  0000 L CNN
F 2 "" H 6325 6350 50  0001 C CNN
F 3 "" H 6325 6350 50  0001 C CNN
	1    6325 6350
	-1   0    0    1   
$EndComp
Wire Wire Line
	6325 5425 6325 5500
Wire Wire Line
	6325 5750 6675 5750
Text GLabel 6725 6050 2    50   Input ~ 0
RS485_A
Text GLabel 6725 5750 2    50   Input ~ 0
RS485_B
Text GLabel 6675 5750 1    50   Input ~ 0
1.4
Text GLabel 6675 6050 3    50   Input ~ 0
1.2
$Comp
L Connector_Generic:Conn_01x03 J12
U 1 1 6013BBC5
P 4725 5900
F 0 "J12" H 4643 6217 50  0000 C CNN
F 1 "Conn_01x03" H 4643 6126 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 4725 5900 50  0001 C CNN
F 3 "~" H 4725 5900 50  0001 C CNN
	1    4725 5900
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5300 5750 4925 5750
Wire Wire Line
	4925 5750 4925 5800
Wire Wire Line
	5300 6050 4925 6050
Wire Wire Line
	4925 6050 4925 6000
Wire Wire Line
	4900 5900 4925 5900
Wire Wire Line
	5250 5900 5250 5850
Wire Wire Line
	5250 5850 5300 5850
Connection ~ 4925 5900
Wire Wire Line
	4925 5900 5250 5900
Wire Wire Line
	5300 5950 5250 5950
Wire Wire Line
	5250 5950 5250 5900
Connection ~ 5250 5900
$Comp
L Connector_Generic:Conn_01x02 J13
U 1 1 6017622E
P 7350 5950
F 0 "J13" H 7268 5625 50  0000 C CNN
F 1 "Conn_01x02" H 7268 5716 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 7350 5950 50  0001 C CNN
F 3 "~" H 7350 5950 50  0001 C CNN
	1    7350 5950
	1    0    0    1   
$EndComp
Wire Wire Line
	6675 5750 6675 5850
Wire Wire Line
	6675 5850 7150 5850
Connection ~ 6675 5750
Wire Wire Line
	6675 5750 6725 5750
Wire Wire Line
	6675 6050 6675 5950
Wire Wire Line
	6675 5950 7150 5950
Connection ~ 6675 6050
Wire Wire Line
	6675 6050 6725 6050
$EndSCHEMATC
